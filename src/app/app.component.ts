import { Component, OnInit } from '@angular/core';
import { FileUploadService } from "src/app/file-upload.service";
import { IStoredFile } from "src/app/stored-file.model";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  storedFiles: IStoredFile[] = [];
  title = 'file-scanner-web';

  constructor(protected uploadService: FileUploadService) {
  }

  ngOnInit(): void {
    this.loadAll()
  }

  delete(id: number) {
    console.log("deleting . . .");
    this.uploadService.delete(id).toPromise();
    this.loadAll();
  }

  loadAll(): void {
    this.uploadService.findAll().subscribe(
      value => {
        console.log('Value fetched ', value);
        this.storedFiles = value.data;
      },
      error => {
        console.log('Error raised ', error);
      })
  }

  trackId(index: number, item: IStoredFile): number {
    return item.id!;
  }
}
