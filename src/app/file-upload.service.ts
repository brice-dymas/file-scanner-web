import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { IApiResponse } from "src/app/api-reponse.model";
import { environment } from "src/environments/environment";



const SERVER_URL = environment.SERVER_URL + '/files'

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(protected http: HttpClient) {
  }

  findAll(): Observable<IApiResponse> {
    return this.http.get<IApiResponse>(SERVER_URL + '/list');
  }

  delete(id: number): Observable<IApiResponse> {
    return this.http.delete<IApiResponse>(`${SERVER_URL}/${id}/delete`);
  }

  download(id: number): Observable<any> {
    return this.http.get<any>(`${SERVER_URL}/${id}/download`);
  }

  upload(firstName: string, lastName: string, file: File): Observable<IApiResponse> {

    // Create form data
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append("file", file, file.name);
    formData.append("firstName", firstName);
    formData.append("lastName", lastName);

    // Make http post request over api
    // with formData as req
    return this.http.post<IApiResponse>(SERVER_URL + '/upload/bad-practice', formData)
  }
}
