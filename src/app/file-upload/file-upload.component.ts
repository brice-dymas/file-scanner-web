import { Component, OnInit } from '@angular/core';
import { FileUploadService } from "src/app/file-upload.service";



@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  firstName: string = '';
  lastName: string = '';
  message: string = '';
  errorRaised = false;
  warningRaised = false;
  succeeded = false;
  // Variable to store shortLink from api response
  shortLink: string = "";
  loading: boolean = false; // Flag variable
  file: File | any; // Variable to store file

  // Inject service
  constructor(protected uploadService: FileUploadService) {
  }

  ngOnInit(): void {
  }

  // On file Select
  // @ts-ignore
  onChange(event) {
    this.file = event.target.files[0];
  }

  // OnClick of button Upload
  onUpload() {
    if (!this.hasErrors()) {
      console.log('Now saving . . .')
      this.loading = !this.loading;
      console.log(this.file);
      this.uploadService.upload(this.firstName, this.lastName, this.file).subscribe(
        value => {
          this.errorRaised = false
          this.succeeded = true
          this.message = value.message;
          this.lastName = '';
          this.firstName = '';

          this.file = null
        },
        error => {
          this.errorRaised = true
          this.message = error.message;
        }
      );
    }
  }

  hasErrors(): boolean {
    this.warningRaised = false;
    if (this.lastName.length < 3 || this.lastName.length < 3) {
      this.warningRaised = true;
      this.message = 'The first and last names must have at least 03 characters each one!';
    }
    if (this.file == null) {
      this.warningRaised = true;
      this.message = 'Please select a file to upload!';
    }
    return this.warningRaised;
  }
}
