export interface IApiResponse {
  success: boolean;
  message: string;
  data?: any;
}

export class ApiResponse implements IApiResponse {
  constructor(
    public success: boolean,
    public message: string,
    public data?: any
  ) {
  }
}
