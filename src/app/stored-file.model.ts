export interface IStoredFile {
  id: number;
  firstName?: string;
  lastName?: string;
  fileName?: string;
  url?: string;
  createdOn?: Date;
  size?: string;
}

export class StoredFile implements IStoredFile {
  constructor(
    public id: number,
    public firstName?: string,
    public lastName?: string,
    public fileName?: string,
    public url?: string,
    public createdOn?: Date,
    public size?: string
  ) {
  }
}

export function getIdentifier(storedFile: IStoredFile): number {
  return storedFile.id;
}
